package wellsaid.it.bakingapp.utils;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import wellsaid.it.bakingapp.R;
import wellsaid.it.bakingapp.ui.RecipeStepsActivity;

/**
 * The adapter for the recipes grid view
 * @author Benedetto Girgenti
 */
public class RecipeAdapter extends RecyclerView.Adapter {

    /* the context from which the adapter in instanciated */
    private Context context;

    /* the list of recipes maintained */
    private List<Recipe> recipes;

    /**
     * Class constructor. It needs the context in which is instanciated
     * in order to inflate views and populate them in the grid view.
     * @param context
     *     The context from which the class is instanciated
     */
    public RecipeAdapter(Context context){
        this.context = context;

        this.recipes = new ArrayList<>();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recipe_item, null);


        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof RecipeAdapter.ViewHolder){
            /* populate the holder */
            final Recipe recipe = recipes.get(position);

            RecipeAdapter.ViewHolder recipeHolder = (RecipeAdapter.ViewHolder) holder;

            if(recipe.imageURL.equals("")){
                Picasso.with(context)
                        .load(R.drawable.recipe_image_placeholder)
                        .into(recipeHolder.recipeImageView);
            } else {
                Picasso.with(context)
                        .load(recipe.imageURL)
                        .placeholder(R.drawable.recipe_image_placeholder)
                        .into(recipeHolder.recipeImageView);
            }

            recipeHolder.recipeNameTextView.setText(recipe.name);

            /* define onclick listener for the element */
            recipeHolder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, RecipeStepsActivity.class);
                    intent.putExtra(RecipeStepsActivity.RECIPE, Parcels.wrap(recipe));
                    context.startActivity(intent);
                }
            });
        } else {
            throw new IllegalArgumentException();
        }
    }

    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder {

        View view;

        @BindView(R.id.recipe_name_text_view)
        TextView recipeNameTextView;
        @BindView(R.id.recipe_image_view)
        ImageView recipeImageView;

        ViewHolder(View view) {
            super(view);

            this.view = view;

            ButterKnife.bind(this, view);
        }
    }

    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return recipes.size();
    }

    /**
     * Adds all the new recipes passed through the list "newRecipes"
     * and notifies the adapter of the change
     * @param newRecipes
     *     The new recipes to add
     */
    public void addAll(List<Recipe> newRecipes){
        recipes.addAll(newRecipes);
        notifyDataSetChanged();
    }
}
