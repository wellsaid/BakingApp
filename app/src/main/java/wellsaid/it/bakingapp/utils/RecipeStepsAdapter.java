package wellsaid.it.bakingapp.utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import wellsaid.it.bakingapp.R;

/**
 * The adapter for the recipes grid view
 * @author Benedetto Girgenti
 */
public class RecipeStepsAdapter extends RecyclerView.Adapter {

    /* the context from which the adapter in instanciated */
    private Context context;

    /* the recipe which will contains the steps */
    private Recipe recipe;

    /**
     * Interface to receive adapter events
     */
    public interface Listener {
        /**
         * Called when the user tap on a recipe step
         * @param ID
         *     The ID of the tapped step
         */
        void onRecipeStepPressed(int ID);
    }

    /* the listener to which send adapter events */
    private Listener listener;

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recipe_item_step, null);


        RecipeStepsAdapter.ViewHolder viewHolder
                = new RecipeStepsAdapter.ViewHolder(itemLayoutView);
        return viewHolder;
    }

    /**
     * Class constructor. It needs the context in which is instanciated
     * in order to inflate views and populate them in the grid view.
     * @param context
     *     The context from which the class is instanciated
     */
    public RecipeStepsAdapter(Context context, Listener listener){
        this.context = context;

        this.listener = listener;

        this.recipe = null;
    }

    /**
     * Returns the ID of the i-th element in the adapter.
     * @param i
     *     The num. of the requested element ID.
     * @return
     *     The ID of the i-th element in the adapter.
     */
    @Override
    public long getItemId(int i) {
        return recipe.steps.get(i).ID;
    }


    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder {

        View view;

        @BindView(R.id.recipe_step_name_text_view)
        TextView recipeStepNameTextView;

        ViewHolder(View view) {
            super(view);

            this.view = view;

            ButterKnife.bind(this, view);
        }
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        RecipeStepsAdapter.ViewHolder recipeStepHolder = (RecipeStepsAdapter.ViewHolder) holder;

        /* Get the recipe step of this element */
        final Recipe.Step step = recipe.steps.get(position);

        /* populate the view */
        recipeStepHolder.recipeStepNameTextView.setText(step.shortDescription);

        /* define onclick listener for the element */
        recipeStepHolder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(listener != null){
                    listener.onRecipeStepPressed(step.ID);
                }
            }
        });
    }

    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return (recipe.steps == null) ? 0 : recipe.steps.size();
    }

    /**
     * Change the recipe of which we have to show the steps
     * @param recipe
     *     The new recipe to show the steps
     */
    public void changeRecipe(Recipe recipe){
        this.recipe = recipe;
        notifyDataSetChanged();
    }
}
