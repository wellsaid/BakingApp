package wellsaid.it.bakingapp.utils;

import android.support.annotation.NonNull;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * A Parcelable class which contains a Recipe
 * @author Benedetto Girgenti
 */
@Parcel
public class Recipe implements Comparable<Recipe> {

    /**
     * A class which contains a Recipe steps
     * @author Benedetto Girgenti
     */
    @Parcel
    public static class Step implements Comparable<Step> {
        public int ID;
        public String shortDescription; /* Description to show in step selector */
        public String description; /* Longer description to show below the video */
        public String videoURL;
        public String thumbnailURL;

        /**
         * Empty Constructor: Needed by the parceler library
         */
        public Step(){}

        /**
         * Constructor
         * @param ID
         *    The ID of the recipe step
         * @param shortDescription
         *    Description which will be shown in step selector
         * @param description
         *    Longer description which will be shown below the video
         * @param videoURL
         *    URL to the video of the recipe step
         * @param thumbnailURL
         *    URL to the thumbnail of the recipe step
         */
        public Step(int ID, String shortDescription, String description,
                    String videoURL, String thumbnailURL){
            this.ID = ID;
            this.shortDescription = shortDescription;
            this.description = description;
            this.videoURL = videoURL;
            this.thumbnailURL = thumbnailURL;
        }

        /**
         * Returns a number representing a comparison of the two objects
         * @param other
         *     The other object with which we want to compare this
         * @return
         *     less than 0 if this < other, 0 if this = other, more then 0 if this > other
         */
        @Override
        public int compareTo(@NonNull Step other) {
            return Integer.compare(this.ID, other.ID);
        }
    }

    /**
     * A class which contains recipe ingredients
     * @author Benedetto Girgenti
     */
    @Parcel
    public static class Ingredient {
        public String name;
        public String measure;
        public float quantity;

        /**
         * Empty Constructor: Needed by the parceler library
         */
        public Ingredient(){}

        /**
         * Constructor
         * @param name
         *     The name of the ingredient
         * @param measure
         *     The measure for the quantity
         * @param quantity
         *     The quantity of ingredient
         */
        public Ingredient(String name, String measure, float quantity){
            this.name = name;
            this.measure = measure;
            this.quantity = quantity;
        }
    }

    public int ID;
    public String name;
    public String imageURL;

    public ArrayList<Step> steps = null;
    public ArrayList<Ingredient> ingredients = null;

    /**
     * Empty Constructor: Needed by the parceler library
     */
    Recipe(){}

    /**
     * Constructor
     * @param ID
     *    The ID of the recipe
     * @param name
     *    The name of the recipe
     * @param imageURL
     *    The url to an image representing the recipe
     */
    public Recipe(int ID, String name, String imageURL){
        this.ID = ID;
        this.name = name;
        this.imageURL = imageURL;
        this.steps = null;
        this.ingredients = null;
    }

    /**
     * Adds a step in the recipe
     * @param ID
     *    The ID of the recipe step
     * @param shortDescription
     *    Description which will be shown in step selector
     * @param description
     *    Longer description which will be shown below the video
     * @param videoURL
     *    URL to the video of the recipe step
     * @param thumbnailURL
     *    URL to the thumbnail of the recipe step
     */
    public void addStep(int ID, String shortDescription, String description,
                   String videoURL, String thumbnailURL){
        if(steps == null){
            steps = new ArrayList<>();
        }

        steps.add(new Step(ID, shortDescription, description, videoURL, thumbnailURL));
    }

    /**
     * Adds an ingredient in the recipe
     * @param name
     *     The name of the ingredient
     * @param measure
     *     The measure for the quantity
     * @param quantity
     *     The quantity of ingredient
     */
    public void addIngredient(String name, String measure, float quantity){
        if(ingredients == null){
            ingredients = new ArrayList<>();
        }

        ingredients.add(new Ingredient(name, measure, quantity));
    }

    @Override
    public int compareTo(@NonNull Recipe recipe) {
        return this.name.compareTo(recipe.name);
    }
}
