package wellsaid.it.bakingapp.utils;

import android.content.Context;
import android.util.DisplayMetrics;

/**
 * Static class to determine which kind of device this is
 * @author Benedetto Girgenti
 */
public class DeviceType {

    /* The possible devices */
    public static final int PHONE = 1;
    public static final int TABLET = 2;

    /**
     * It returns which device this is
     * @param context
     *     The needed context to get the display metrics
     * @return
     *     The code of this device.
     *     Possible values are: DeviceType.PHONE, DeviceType.TABLET
     */
    public static int getType(Context context){
        /* Determine display width in dp */
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        float dpHeight = displayMetrics.heightPixels / displayMetrics.density;
        float sw = (dpWidth <= dpHeight)?dpWidth:dpHeight;

        if(sw >= 600){ /* we are on tablet */
            return TABLET;
        } else { /* we are on a phone */
            return PHONE;
        }
    }
}
