package wellsaid.it.bakingapp.utils;

import android.content.Context;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import wellsaid.it.bakingapp.R;

/**
 * Class that retrieves recipes from the server
 */
public class RecipesGetter {

    private static final String LOG_TAG = RecipesGetter.class.getSimpleName();

    /**
     * The interface to be implemented by the caller who wants the result back
     */
    public interface Listener {
        void onRecipesResponse(ArrayList<Recipe> recipesList);
        void onRecipeStepsResponse(Recipe recipe);
        void onRecipeIngredientsResponse(Recipe recipe);
    }

    /**
     * Retrieves the recipes name and image from the server and call the listener when done
     * @param context
     *     The context from which is called (needed to get the URL of the recipes json)
     * @param listener
     *     The listener to which send the response
     */
    public static void getRecipes(final Context context, final Listener listener){
        /* Create and start the async client which will get the response */
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(
                context.getString(R.string.recipes_json_url),
                new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        ArrayList<Recipe> recipesList = new ArrayList<>();

                        String responseString = new String(responseBody, StandardCharsets.UTF_8);
                        JSONArray jsonArray = null;
                        try {
                            jsonArray = new JSONArray(responseString);

                            /* for each recipe in the server */
                            for(int i=0; i < jsonArray.length(); i++){
                                /* add it to the list */
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                int ID = jsonObject
                                        .getInt(context.getString(R.string.recipe_id_json_key));
                                String name = jsonObject
                                        .getString(context.getString(R.string.recipe_name_json_key));
                                String imageURL = jsonObject
                                        .getString(context.getString(R.string.recipe_image_url_json_key));

                                Recipe newRecipe = new Recipe(ID,name,imageURL);
                                recipesList.add(newRecipe);
                            }
                        } catch(JSONException e){
                            Log.e(LOG_TAG, "Error parsing the response: " + e.getMessage());
                        } finally {
                            /* return the list to the listener */
                            listener.onRecipesResponse(recipesList);
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers,
                                          byte[] responseBody, Throwable error) {
                        /* return null to the listener to signal an error */
                        listener.onRecipesResponse(null);
                    }
                }
        );
    }

    /**
     * Retrieves the recipe steps from the server and call the listener when done
     * @param context
     *     The context from which is called (needed to get the URL of the recipes json)
     * @param listener
     *     The listener to which send the response
     * @param recipe
     *     The recipe to which it will populate the steps
     */
    public static void getRecipeSteps(final Context context, final Listener listener,
                                      final Recipe recipe){
        /* Create and start the async client which will get the response */
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(
                context.getString(R.string.recipes_json_url),
                new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        String responseString = new String(responseBody, StandardCharsets.UTF_8);
                        JSONArray jsonArray = null;
                        try {
                            jsonArray = new JSONArray(responseString);

                            /* for each recipe in the server */
                            JSONObject jsonObject = null;
                            int i=0; for(; i < jsonArray.length(); i++){
                                /* check if we have reached the desired recipe */
                                jsonObject = jsonArray.getJSONObject(i);
                                int ID = jsonObject
                                        .getInt(context.getString(R.string.recipe_id_json_key));
                                if(ID == recipe.ID){
                                    /* then stop */
                                    break;
                                }
                            }

                            /* if we have not reached the end of the list */
                            if(i != jsonArray.length()){
                                /* retrieve recipe steps and add them to the recipe */
                                jsonArray = jsonObject
                                        .getJSONArray(context.getString(R.string.recipe_steps_json_key));

                                for(i=0; i < jsonArray.length(); i++) {
                                    jsonObject = jsonArray.getJSONObject(i);
                                    int ID = jsonObject
                                            .getInt(context.getString(R.string.recipe_step_id_json_key));
                                    String shortDescription = jsonObject
                                            .getString(context.getString(R.string.recipe_step_short_desc_json_key));
                                    String description = jsonObject
                                            .getString(context.getString(R.string.recipe_step_desc_json_key));
                                    String videoURL = jsonObject
                                            .getString(context.getString(R.string.recipe_step_video_url_json_key));
                                    String thumbnailURL = jsonObject
                                            .getString(context.getString(R.string.recipe_step_thumbnail_url_json_key));

                                    recipe.addStep(ID, shortDescription, description,
                                                              videoURL, thumbnailURL);
                                }
                            }
                        } catch(JSONException e){
                            Log.e(LOG_TAG, "Error parsing the response: " + e.getMessage());
                        } finally {
                            listener.onRecipeStepsResponse(recipe);
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers,
                                          byte[] responseBody, Throwable error) {
                        listener.onRecipeStepsResponse(recipe);
                    }
                }
        );
    }

    /**
     * Retrieves the recipe ingredients from the server and call the listener when done
     * @param context
     *     The context from which is called (needed to get the URL of the recipes json)
     * @param listener
     *     The listener to which send the response
     * @param recipe
     *     The recipe to which it will populate the steps
     */
    public static void getRecipeIngrediest(final Context context, final Listener listener,
                                      final Recipe recipe){
        /* Create and start the async client which will get the response */
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(
                context.getString(R.string.recipes_json_url),
                new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        String responseString = new String(responseBody, StandardCharsets.UTF_8);
                        JSONArray jsonArray = null;
                        try {
                            jsonArray = new JSONArray(responseString);

                            /* for each recipe in the server */
                            JSONObject jsonObject = null;
                            int i=0; for(; i < jsonArray.length(); i++){
                                /* check if we have reached the desired recipe */
                                jsonObject = jsonArray.getJSONObject(i);
                                int ID = jsonObject
                                        .getInt(context.getString(R.string.recipe_id_json_key));
                                if(ID == recipe.ID){
                                    /* then stop */
                                    break;
                                }
                            }

                            /* if we have not reached the end of the list */
                            if(i != jsonArray.length()){
                                /* retrieve recipe steps and add them to the recipe */
                                jsonArray = jsonObject
                                        .getJSONArray(context.getString(R.string.recipe_ingredients_json_key));

                                for(i=0; i < jsonArray.length(); i++) {
                                    jsonObject = jsonArray.getJSONObject(i);
                                    String name = jsonObject
                                            .getString(context.getString(R.string.recipe_ingredient_name_json_key));
                                    String measure = jsonObject
                                            .getString(context.getString(R.string.recipe_ingredient_measure_json_key));
                                    float quantity = (float) jsonObject
                                            .getDouble(context.getString(R.string.recipe_ingredient_quantity_json_key));

                                    recipe.addIngredient(name, measure, quantity);
                                }
                            }
                        } catch(JSONException e){
                            Log.e(LOG_TAG, "Error parsing the response: " + e.getMessage());
                        } finally {
                            listener.onRecipeIngredientsResponse(recipe);
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers,
                                          byte[] responseBody, Throwable error) {
                        listener.onRecipeIngredientsResponse(recipe);
                    }
                }
        );
    }
}
