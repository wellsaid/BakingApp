package wellsaid.it.bakingapp.ui;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import wellsaid.it.bakingapp.R;

/**
 * Implementation of App Widget functionality.
 */
public class RecipeStepsWidget extends AppWidgetProvider {

    public static final String RECIPE = "recipe";

    private static void updateAppWidget(final Context context, final AppWidgetManager appWidgetManager,
                                        final int appWidgetId) {
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.recipe_steps_widget);

        /* On click start the select recipe activity */
        Intent intent = new Intent(context, SelectRecipeActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
        views.setOnClickPendingIntent(R.id.recipe_steps_widget_root, pendingIntent);

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        /* There may be multiple widgets active, so update all of them */
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        /* Nothing to be done here */
    }

    @Override
    public void onDisabled(Context context) {
        /* Nothing to be done here */
    }
}

