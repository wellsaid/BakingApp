package wellsaid.it.bakingapp.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.squareup.picasso.Picasso;

import org.parceler.Parcels;

import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import wellsaid.it.bakingapp.R;
import wellsaid.it.bakingapp.utils.DeviceType;
import wellsaid.it.bakingapp.utils.Recipe;

/**
 * The Fragment which will contain the recipe step
 * @Author Benedetto Girgenti
 */
public class ViewRecipeStepFragment extends Fragment implements Player.EventListener {
    /* the fragment initialization parameters */
    private static final String RECIPE = "recipe";
    private static final String PLAYER_START_POSITION = "position";
    private static final String PLAYER_STATUS = "status";

    /* the recipe to which we must show the steps */
    private Recipe recipe = null;

    /* The context from which the fragment is initialized. */
    private Context context;

    /* The ID of the step we are showing */
    private int ID;

    /* The player which will reproduce the video */
    private ExoPlayer player;

    /* The position from where player should start in milliseconds */
    private long milli = -1;
    private boolean playWhenReady = true;

    /* Binding all the views in the fragment */
    @BindView(R.id.step_video_player_view)
    PlayerView playerView;

    @BindView(R.id.description_text_view)
    TextView descriptionTextView;

    @BindView(R.id.next_step_button)
    Button nextStepButton;

    @BindView(R.id.step_thumbnail_image_view)
    ImageView thumbnailImageView;

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {
        /* if is loading and we have a saved position */
        if(isLoading && milli != -1){
            /* start from it */
            player.seekTo(milli);
        }
    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    public interface Listener {
    }

    /* the listener to perform fragment interaction */
    private Listener listener;

    /**
     * Empty Constructor. (Required)
     */
    public ViewRecipeStepFragment() {}

    /**
     * Sets the context for this fragment
     * @param context
     *     The context from which the fragment is initialized.
     */
    public void setContext(Context context){
        this.context = context;
    }

    /**
     * Sets the position from where player should start in milliseconds
     * @param milli
     *     The position from where player should start in milliseconds
     */
    public void setPlayerPosition(long milli){
        this.milli = milli;
    }

    /**
     * Sets the step ID to show
     * @param ID
     *     The step ID to show
     */
    public void setStepID(int ID){
        this.ID = ID;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param recipe
     *     The recipe to which we must show the steps
     * @param context
     *     The context from which the fragment is initialized.
     * @param milli
     *     The position from where player should start in milliseconds
     * @param ID
     *     The step ID to show
     * @return
     *     A new instance of fragment RecipeStepsFragment.
     */
    public static ViewRecipeStepFragment newInstance(Recipe recipe, Context context,
                                                     long milli, int ID) {
        ViewRecipeStepFragment fragment = new ViewRecipeStepFragment();
        fragment.setContext(context);
        fragment.setPlayerPosition(milli);
        fragment.setStepID(ID);
        Bundle args = new Bundle();
        args.putParcelable(RECIPE, Parcels.wrap(recipe));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            recipe = Parcels.unwrap(getArguments().getParcelable(RECIPE));
        }

        if(savedInstanceState != null){
            milli = savedInstanceState.getLong(PLAYER_START_POSITION, -1);
            playWhenReady = savedInstanceState.getBoolean(PLAYER_START_POSITION, true);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putLong(PLAYER_START_POSITION, player.getContentPosition());
        outState.putBoolean(PLAYER_STATUS, player.getPlayWhenReady());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        /* Putting image in the thumbnail is handled in the showStep() method below */

        if(context == null)
            return null;

        /* Inflate the layout for this fragment */
        View view = inflater.inflate(R.layout.fragment_view_recipe_step,
                container, false);

        /* Bind the defined views */
        ButterKnife.bind(this, view);

        /* Create a default TrackSelector */
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector =
                new DefaultTrackSelector(videoTrackSelectionFactory);

        /* Create the video player */
        player =
                ExoPlayerFactory.newSimpleInstance(context, trackSelector);

        /* Make it autostart when ready */
        player.setPlayWhenReady(playWhenReady);

        /* Set this fragment as listener for player events */
        player.addListener(this);

        /* Attach it to the PlayerView */
        playerView.setPlayer(player);

        /* if we are on a phone */
        if(DeviceType.getType(context) == DeviceType.PHONE){
            /* show the next step button */
            nextStepButton.setVisibility(View.VISIBLE);
        }

        /* show the step on the fragment */
        showStep(ID);

        /* Return the view to the caller */
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Listener) {
            listener = (Listener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement " + ViewRecipeStepFragment.class.getSimpleName() + "."
                    + ViewRecipeStepFragment.Listener.class.getSimpleName());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
        if(player != null) {
            player.stop();
            player.release();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(player != null){
            player.stop();
            player.release();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if(player != null){
            player.stop();
            player.release();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(player != null){
            player.setPlayWhenReady(true);
        }
    }

    /**
     * Called when the next step button is pressed
     * @param view
     *     A reference to the clicked view
     */
    public void onNextStepButtonClick(View view){
        if(this.ID != Collections.max(recipe.steps).ID){
            showStep(++this.ID);
        }
    }

    /**
     * Returns the player position in milliseconds
     * @return
     *     The player position in milliseconds
     */
    public long getPlayerPosition(){
        return player.getCurrentPosition();
    }

    /**
     * Show the passed step in the fragment
     * WARNING: It must be called after onCreate() has been called on the fragment.
     * Otherwise no effect.
     * @param ID
     *     The ID of the step to show
     */
    public void showStep(int ID){
        /* reset saved position */
        milli = -1;

        /* If we not have the recipe (onCreate() not called) */
        if(recipe == null){
            /* stop immediately */
            return;
        }

        /* If we are on a phone */
        if(DeviceType.getType(context) == DeviceType.PHONE){
            /* If we are going to show the last step */
            if(ID == Collections.max(recipe.steps).ID){
                /* Hide the next step button */
                nextStepButton.setVisibility(View.GONE);
            }
        }

        /* Select the step from the recipe object */
        Recipe recipeKey = new Recipe(0, "", "");
        recipeKey.addStep(ID, "", "", "", "");
        Recipe.Step stepKey = recipeKey.steps.get(0);
        Recipe.Step stepToShow = recipe.steps.get(Collections.binarySearch(recipe.steps, stepKey));

        if(stepToShow == null)
            throw new IllegalArgumentException();

        /* if there is no video source */
        if(stepToShow.videoURL.equals("")){
            /* hide the player */
            player.stop();
            playerView.setVisibility(View.GONE);
        } else {
            /* show the player */
            playerView.setVisibility(View.VISIBLE);

            /* Start playing the video */
            DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(context,
                    Util.getUserAgent(context, "yourApplicationName"), bandwidthMeter);
            MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                    .createMediaSource(Uri.parse(stepToShow.videoURL));

            player.prepare(videoSource);

            /* if we are on a phone */
            if(DeviceType.getType(this.getContext()) == DeviceType.PHONE){
                /* if we are in landscape mode */
                if(getResources().getConfiguration().orientation
                        == Configuration.ORIENTATION_LANDSCAPE){
                    /* set the player fullscreen */
                    setPlayerFullscreen();
                }
            }
        }

        if(!stepToShow.thumbnailURL.equals("")){
            /* load the image in the image view */
            Picasso.with(context)
                    .load(stepToShow.thumbnailURL)
                    .placeholder(R.drawable.recipe_image_placeholder)
                    .into(thumbnailImageView);

            /* show the image view */
            thumbnailImageView.setVisibility(View.VISIBLE);
        } else {
            /* hide the image view */
            thumbnailImageView.setVisibility(View.GONE);
        }

        /* Add step description */
        descriptionTextView.setText(stepToShow.description);
    }

    private void setPlayerFullscreen(){
        Dialog fullScreenDialog =
                new Dialog(this.getContext(), android.R.style.Theme_Black_NoTitleBar_Fullscreen);

        ((ViewGroup) playerView.getParent()).removeView(playerView);
        fullScreenDialog.addContentView(playerView,
                new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));

        fullScreenDialog.show();
    }
}
