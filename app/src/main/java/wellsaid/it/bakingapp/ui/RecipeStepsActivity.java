package wellsaid.it.bakingapp.ui;

import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.test.espresso.IdlingResource;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RemoteViews;

import org.parceler.Parcels;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import wellsaid.it.bakingapp.R;
import wellsaid.it.bakingapp.utils.DeviceType;
import wellsaid.it.bakingapp.utils.Recipe;
import wellsaid.it.bakingapp.utils.RecipesGetter;
import wellsaid.it.bakingapp.utils.SimpleIdlingResource;

public class RecipeStepsActivity extends AppCompatActivity
        implements RecipeStepsFragment.Listener, ViewRecipeStepFragment.Listener,
                   RecipesGetter.Listener {

    /* Activity extras to be received */
    public static final String RECIPE = "recipe";
    public static final String WATCHING_ID = "watchingRecipeStepOnPhone";
    public static final String PLAYER_POSITION = "playerPosition";

    /* The recipe to which we have to show the steps */
    private Recipe recipe;

    /* The fragment manager used to create fragments */
    private FragmentManager fragmentManager;

    /* The fragment currently contained in container 1 */
    private Fragment fragment1;

    /* The fragment currently contained in container 2 */
    private Fragment fragment2;

    /* The position from where player should start in milliseconds */
    private long playerPosition = -1;

    /* -1 if we were watching the steps list, the ID of the watching step otherwise */
    private int watchingID = -1;

    /* The recipes list to show */
    private ArrayList<Recipe> recipesList;

    @Nullable
    private SimpleIdlingResource idlingResource;

    @VisibleForTesting
    @NonNull
    public IdlingResource getIdlingResource() {
        if (idlingResource == null) {
            idlingResource = new SimpleIdlingResource();
        }
        return idlingResource;
    }

    /**
     * Called by Android when the activity is started
     *
     * @param savedInstanceState Bundle which containts eventual saved state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_steps);

        /* Bind the defined views */
        ButterKnife.bind(this);

        /* Retrieve extras if they exist */
        Intent intent = getIntent();
        if (intent.hasExtra(RECIPE)) {
            recipe = Parcels.unwrap(intent.getParcelableExtra(RECIPE));
        } else {
            throw new IllegalArgumentException();
        }

        if(savedInstanceState != null){
            watchingID = savedInstanceState.getInt(WATCHING_ID);
            playerPosition = savedInstanceState.getLong(PLAYER_POSITION);
        }

        /* Start retrieving ingredients */
        RecipesGetter.getRecipeIngrediest(this, this, recipe);

        /* Put recipe name in the title and set home button */
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(recipe.name);
        actionBar.setDisplayOptions(R.menu.recipe_step_activity_options);

        /* Get the fragment manager to create fragments */
        fragmentManager = getSupportFragmentManager();

        /* if we have no recipe steps */
        if(recipe.steps == null){
            /* start retrieving recipe steps */
            RecipesGetter.getRecipeSteps(this, this, recipe);
        } else {
            /* otherwise: call the received step handler (we already have them) */
            onRecipeStepsResponse(recipe);
        }
    }

    /**
     * Called when the user tap on a recipe step
     *
     * @param ID The ID of the tapped step
     */
    @Override
    public void onRecipeStepPressed(int ID) {
        /* if we are on a phone */
        if (DeviceType.getType(this) == DeviceType.PHONE) {
            /* Substitute recipe steps fragment with view step one */
            fragment1 = ViewRecipeStepFragment.newInstance(recipe, this, playerPosition, ID);
            fragmentManager.beginTransaction()
                    .replace(R.id.fragment_container_1, fragment1)
                    .commit();

            watchingID = ID;
        /* if we are on a tablet */
        } else if (DeviceType.getType(this) == DeviceType.TABLET) {
            /* Change content of view step fragment */
            ((ViewRecipeStepFragment) fragment2).showStep(ID);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.recipe_step_activity_options, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                NavUtils.navigateUpFromSameTask(this);
                break;
            default:
                break;
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        /* if we are on a phone */
        if (DeviceType.getType(this) == DeviceType.PHONE) {
            /* if we are watching a step -> go previous step until return to steps */
            if(fragment1 instanceof ViewRecipeStepFragment){
                if(watchingID != 0) {
                    /* show the previous step */
                    fragment1 = ViewRecipeStepFragment.newInstance(recipe,
                            this, playerPosition, --watchingID);
                    fragmentManager.beginTransaction()
                            .replace(R.id.fragment_container_1, fragment1)
                            .commit();
                } else {
                    /* Substitute the view step fragment with the recipe steps one */
                    fragment1 = RecipeStepsFragment.newInstance(recipe);
                    fragmentManager.beginTransaction()
                            .replace(R.id.fragment_container_1, fragment1)
                            .commit();

                    watchingID = -1;
                }
            /* if we are watching steps -> go back to recipe selector */
            } else {
                super.onBackPressed();
                finish();
            }
        /* if we are on a tablet */
        } else if (DeviceType.getType(this) == DeviceType.TABLET) {
            super.onBackPressed();
            finish();
        }
    }

    /**
     * Called when the next step button is pressed
     * @param view
     *     A reference to the clicked view
     */
    public void onNextStepButtonClick(View view){
        /* Call the handler in the fragment */
        ((ViewRecipeStepFragment) fragment1).onNextStepButtonClick(view);
        watchingID++;
    }

    /**
     * Called by RecipesGetter when the recipes has been downloaded
     * @param recipesList
     *     The list of downloaded recipes
     */
    @Override
    public void onRecipesResponse(ArrayList<Recipe> recipesList) {
        /* Nothing to do here */
    }

    /**
     * Called by RecipesGetter when the recipe Steps has been downloaded
     * @param recipe
     *     The updated recipe object
     */
    @Override
    public void onRecipeStepsResponse(Recipe recipe) {
        if(watchingID != -1){
            /* Create view recipe step fragment */
            fragment1 = ViewRecipeStepFragment.newInstance(recipe, this,
                    playerPosition, watchingID);
        } else {
            /* Create recipe steps fragment */
            fragment1 = RecipeStepsFragment.newInstance(recipe);
        }
        fragmentManager.beginTransaction()
                .add(R.id.fragment_container_1, fragment1)
                .commit();

        /* If we are on a tablet */
        if (DeviceType.getType(this) == DeviceType.TABLET) {
            fragment2 = ViewRecipeStepFragment.newInstance(recipe, this,
                    playerPosition, 0);
            fragmentManager.beginTransaction()
                    .add(R.id.fragment_container_2, fragment2)
                    .commit();
        }

        if(idlingResource != null){
            idlingResource.setIdleState(true);
        }
    }

    @Override
    public void onRecipeIngredientsResponse(Recipe recipe) {
        /* Send it to the widget */
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this);
        RemoteViews remoteViews = new RemoteViews(this.getPackageName(), R.layout.recipe_steps_widget);
        ComponentName thisWidget = new ComponentName(this, RecipeStepsWidget.class);

        StringBuilder ingredientsStringBuilder = new StringBuilder();
        for(Recipe.Ingredient ingredient : recipe.ingredients){
            ingredientsStringBuilder
                    .append(ingredient.quantity).append(" ")
                    .append(ingredient.measure).append(" ")
                    .append("of ").append(ingredient.name).append("\n\n");
        }

        remoteViews.setTextViewText(R.id.recipe_name_text_view, recipe.name);
        remoteViews.setTextViewText(R.id.recipe_ingredients_text_view, ingredientsStringBuilder.toString());

        appWidgetManager.updateAppWidget(thisWidget, remoteViews);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(WATCHING_ID, watchingID);

        ViewRecipeStepFragment temp = (ViewRecipeStepFragment)
                ((fragment1 instanceof ViewRecipeStepFragment)?fragment1:
                 (fragment2 instanceof ViewRecipeStepFragment)?fragment2:null);
        if(temp != null) {
            outState.putLong(PLAYER_POSITION, temp.getPlayerPosition());
        } else {
            outState.putLong(PLAYER_POSITION, -1);
        }
    }
}