package wellsaid.it.bakingapp.ui;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.parceler.Parcels;

import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import wellsaid.it.bakingapp.R;
import wellsaid.it.bakingapp.utils.DeviceType;
import wellsaid.it.bakingapp.utils.Recipe;
import wellsaid.it.bakingapp.utils.RecipeStepsAdapter;

/**
 * The Fragment which will contain the recipe steps
 * @Author Benedetto Girgenti
 */
public class RecipeStepsFragment extends Fragment implements RecipeStepsAdapter.Listener {

    /* the fragment initialization parameters */
    private static final String RECIPE = "recipe";

    /* the recipe to which we must show the steps */
    private Recipe recipe;

    @BindView(R.id.recipe_steps_list_view)
    RecyclerView recyclerView;

    @BindView(R.id.recipe_ingredients_text_view)
    TextView ingredientsTextView;

    /* The adapter for the above list view */
    RecipeStepsAdapter recipeStepsAdapter;

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    public interface Listener {
        /**
         * Called when the user tap on a recipe step
         * @param ID
         *     The ID of the tapped step
         */
        void onRecipeStepPressed(int ID);
    }

    /* the listener to perform fragment interaction */
    private Listener listener;

    /**
     * Empty Constructor. (Required)
     */
    public RecipeStepsFragment() {}

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param recipe
     *     The recipe to which we must show the steps
     * @return
     *     A new instance of fragment RecipeStepsFragment.
     */
    public static RecipeStepsFragment newInstance(Recipe recipe) {
        RecipeStepsFragment fragment = new RecipeStepsFragment();
        Bundle args = new Bundle();
        args.putParcelable(RECIPE, Parcels.wrap(recipe));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            recipe = Parcels.unwrap(getArguments().getParcelable(RECIPE));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        /* Inflate the layout for this fragment */
        View view = inflater.inflate(R.layout.fragment_recipe_steps, container, false);

        /* Bind the defined views */
        ButterKnife.bind(this, view);

        /* instanciate and set adapter for the gridview */
        recipeStepsAdapter = new RecipeStepsAdapter(this.getContext(), this);
        recyclerView.setAdapter(recipeStepsAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));

        /* add recipe steps to the adapter */
        recipeStepsAdapter.changeRecipe(recipe);

        /* Add ingredients dialog */
        ingredientsTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StringBuilder ingredientsStringBuilder = new StringBuilder();
                for(Recipe.Ingredient ingredient : recipe.ingredients){
                    ingredientsStringBuilder
                            .append(ingredient.quantity).append(" ")
                            .append(ingredient.measure).append(" ")
                            .append("of ").append(ingredient.name).append("\n\n");
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(R.string.ingredients)
                        .setMessage(ingredientsStringBuilder.toString())
                        .setPositiveButton(R.string.ok, null);

                builder.create().show();
            }
        });

        /* Return the view to the caller */
        return view;
    }

    /**
     * Called when the user tap on a recipe step
     * @param ID
     *     The ID of the tapped step
     */
    public void onRecipeStepPressed(int ID) {
        if (listener != null) {
            listener.onRecipeStepPressed(ID);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Listener) {
            listener = (Listener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement " + RecipeStepsFragment.class.getSimpleName() + "."
                    + Listener.class.getSimpleName());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }
}
