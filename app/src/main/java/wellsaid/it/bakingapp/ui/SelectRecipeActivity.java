package wellsaid.it.bakingapp.ui;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.test.espresso.IdlingResource;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import org.parceler.Parcels;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import wellsaid.it.bakingapp.R;
import wellsaid.it.bakingapp.utils.DeviceType;
import wellsaid.it.bakingapp.utils.Recipe;
import wellsaid.it.bakingapp.utils.RecipeAdapter;
import wellsaid.it.bakingapp.utils.RecipesGetter;
import wellsaid.it.bakingapp.utils.SimpleIdlingResource;

/**
 * The first activity of the app it shows a grid of all the available
 * recipes on the server
 * @author Benedetto Girgenti
 */
public class SelectRecipeActivity extends AppCompatActivity implements RecipesGetter.Listener {

    /* Activity extras to be received */
    public static final String RECIPES_LIST = "recipesList";

    /* The textview to show in case of error */
    @BindView(R.id.error_text_view)
    TextView errorTextView;

    /* The grid view which will contain the recipe elements */
    @BindView(R.id.recipes_grid_view)
    RecyclerView recipesRecyclerView;

    /* The adapter for the above grid view */
    private RecipeAdapter recipeAdapter;

    /* The recipes list to show */
    private ArrayList<Recipe> recipesList;

    @Nullable private SimpleIdlingResource idlingResource;

    @VisibleForTesting
    @NonNull
    public IdlingResource getIdlingResource() {
        if (idlingResource == null) {
            idlingResource = new SimpleIdlingResource();
        }
        return idlingResource;
    }

    /**
     * Called by Android when the activity is started
     * @param savedInstanceState
     *     Bundle which containts eventual saved state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_recipe);

        /* Bind the defined views */
        ButterKnife.bind(this);

        /* instanciate and set adapter for the gridview */
        recipeAdapter = new RecipeAdapter(this);
        recipesRecyclerView.setAdapter(recipeAdapter);
        if(DeviceType.getType(this) == DeviceType.PHONE) {
            recipesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        } else if(DeviceType.getType(this) == DeviceType.TABLET) {
            recipesRecyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        }

        /* if we have a recipe list saved */
        if(savedInstanceState != null){
            /* show it */
            ArrayList<Parcelable> recipes = savedInstanceState.getParcelableArrayList(RECIPES_LIST);
            ArrayList<Recipe> recipes1 = new ArrayList<>();
            for(Parcelable recipe : recipes){
                recipes1.add((Recipe) Parcels.unwrap(recipe));
            }

            onRecipesResponse(recipes1);
        } else {
            /* Start getting recipes */
            RecipesGetter.getRecipes(this, this);
        }
    }

    /**
     * Called by RecipesGetter when the recipes has been downloaded
     * @param recipesList
     *     The list of downloaded recipes
     */
    @Override
    public void onRecipesResponse(ArrayList<Recipe> recipesList) {
        this.recipesList = recipesList;

        /* If there was an error getting the recipes ... */
        if(recipesList == null){
            /* ... shows error text view and hide gridview */
            recipesRecyclerView.setVisibility(View.GONE);
            errorTextView.setVisibility(View.VISIBLE);
        } else {
            /* Add all recipes to the adapter */
            recipeAdapter.addAll(recipesList);
        }

        if(idlingResource != null){
            idlingResource.setIdleState(true);
        }
    }

    /**
     * Called by RecipesGetter when the recipe Steps has been downloaded
     * @param recipe
     *     The updated recipe object
     */
    @Override
    public void onRecipeStepsResponse(Recipe recipe) {
        /* Nothing to do here */
    }

    @Override
    public void onRecipeIngredientsResponse(Recipe recipe) {
        /* Nothing to do here */
    }

    /**
     * Called when the activity is rotated
     * @param outState
     *     The state to pass when the activity returns
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (recipesList != null) {
            ArrayList<Parcelable> recipes1 = new ArrayList<>();
            for (Recipe recipe : recipesList) {
                recipes1.add(Parcels.wrap(recipe));
            }

            outState.putParcelableArrayList(RECIPES_LIST, recipes1);
        }
    }
}
