package wellsaid.it.bakingapp;

import android.content.Intent;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.IdlingResource;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.parceler.Parcels;

import wellsaid.it.bakingapp.ui.RecipeStepsActivity;
import wellsaid.it.bakingapp.utils.Recipe;

import static android.support.test.espresso.Espresso.onIdle;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withChild;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;


/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class RecipeStepsActivityInstrumentedTest {

    static final String STEP_NAME = "Recipe Introduction";

    @Rule
    public ActivityTestRule<RecipeStepsActivity> activityTestRule
            = new ActivityTestRule<RecipeStepsActivity>(RecipeStepsActivity.class){
        @Override
        protected Intent getActivityIntent() {
            Intent intent = new Intent();
            Recipe recipe = new Recipe(1,"Nutella Pie","");
            intent.putExtra(RecipeStepsActivity.RECIPE, Parcels.wrap(recipe));
            return intent;
        }
    };

    private IdlingResource idlingResource;

    @Before
    public void registerIdlingResource(){
        idlingResource = activityTestRule.getActivity().getIdlingResource();
        Espresso.registerIdlingResources(idlingResource);
    }

    /**
     * Clicks on a GridView item and checks it opens up the OrderActivity with the correct details.
     */
    @Test
    public void clickListViewItem_OpensRecipeStep() {
        // Uses {@link Espresso#onData(org.hamcrest.Matcher)} to get a reference to a specific
        // gridview item and clicks it.
        onIdle();

        onView(withId(R.id.fragment_container_1))
                .check(matches(withChild(withChild(withChild(withChild(withText(STEP_NAME)))))));
    }

    @After
    public void unregisterIdlingResource(){
        if(idlingResource != null){
            Espresso.unregisterIdlingResources(idlingResource);
        }
    }
}